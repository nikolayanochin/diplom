<?php

use yii\db\Migration;

class m170430_155029_create_access_token extends Migration
{
    const TOKEN = "{{%access_token}}";
    const USER = "{{%user}}";
    const ACCOUNT = "{{%account}}";

    public function up()
    {
        $this->createTable(self::TOKEN, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'state' => $this->string(32)->notNull()->comment('Случайный ключ для определения принадлежности'),

            'token' => $this->string()->comment('Ключ доступа'),
            'account_id' => $this->bigInteger()->comment('Аккаунт'),

            'status' => $this->smallInteger()->notNull()->comment('Статус доступа'),

            'created_at' => $this->integer()->comment('Дата создания'),
            'updated_at' => $this->integer()->comment('Дата редактирования'),
            'expires_at' => $this->integer()->comment('Дата окончания'),
        ]);

        $this->addForeignKey('access_token__user', self::TOKEN, 'user_id', self::USER, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TOKEN);
    }
}
