<?php

use yii\db\Migration;

class m170501_125043_create_account_relation extends Migration
{
    const RELATION = "{{%account_relation}}";
    const ACCOUNT = "{{%account}}";

    public function up()
    {
        $this->createTable(self::RELATION, [
            'id' => $this->bigPrimaryKey(),
            'initiator_id' => $this->bigInteger()->notNull()->comment('Инициатор'),
            'target_id' => $this->bigInteger()->notNull()->comment('Цель'),
        ]);

        $this->addForeignKey('account_relation__initiator', self::RELATION, 'initiator_id', self::ACCOUNT, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('account_relation__target', self::RELATION, 'target_id', self::ACCOUNT, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::RELATION);
    }
}
