<?php

use yii\db\Migration;

class m170430_155000_create_account extends Migration
{
    const ACCOUNT = "{{%account}}";

    public function up()
    {
        $this->createTable(self::ACCOUNT, [
            'id' => $this->bigPrimaryKey(),
            'social_network_id' => $this->smallInteger()->comment('Социальная сеть'),
            'identifier' => $this->string()->notNull()->comment('Идентификатор аккаунта в социальной сети'),

            'display_name' => $this->string(32)->comment('Отображаемое имя'),
            'description' => $this->string()->comment('Биография'),
            'first_name' => $this->string(32)->comment('Имя'),
            'last_name' => $this->string(32)->comment('Фамилия'),

            'gender' => $this->boolean()->comment('Пол'),
            'birth_date' => $this->string()->comment('Дата рождения'),
            'email' => $this->string()->comment('Почта'),
            'phone' => $this->string()->comment('Телефон'),

            'address' => $this->string()->comment('Полный адрес'),

            'country' => $this->string()->comment('Страна'),
            'region' => $this->string()->comment('Регион'),
            'city' => $this->string()->comment('Город'),
            'zip' => $this->string()->comment('Индекс'),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::ACCOUNT);
    }
}
