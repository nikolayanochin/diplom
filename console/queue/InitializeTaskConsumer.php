<?php
/**
 * Created by PhpStorm.
 * User: unregistered
 * Date: 20.06.2017
 * Time: 20:40
 */

namespace console\queue;


use mikemadisonweb\rabbitmq\components\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use yii\base\Object;

class InitializeTaskConsumer extends Object implements ConsumerInterface
{
    /** @inheritdoc */
    public function execute(AMQPMessage $msg)
    {
        echo $msg->body;
        return ConsumerInterface::MSG_ACK;
    }
}