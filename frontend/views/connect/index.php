<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 21.06.2017
 * Time: 12:31
 */

use yii\grid\GridView;

/** @var \yii\web\View $this */
/** @var \common\models\AccessToken|null $model */

$this->title = 'Токены доступа';
?>

<div class="token-index">
    <h1><?= $this->title ?></h1>

    <p>
        <?php if (null !== $model): ?>
            <?php if (!$model->isExpired()): ?>
                Истекает <?= Yii::$app->formatter->asRelativeTime($model->expires_at) ?> (<?= Yii::$app->formatter->asDatetime($model->expires_at) ?>)
            <?php else: ?>
                Срок действия токена истек
                <?= \yii\helpers\Html::a('Обновить токен', ['auth'], ['class' => 'btn btn-success']) ?>
            <?php endif ?>
        <?php else: ?>
            Необходимо получить токен
            <?= \yii\helpers\Html::a('Получить токен', ['auth'], ['class' => 'btn btn-success']) ?>
        <?php endif ?>
    </p>
</div>
