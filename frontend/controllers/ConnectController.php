<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 21.06.2017
 * Time: 11:53
 */

namespace frontend\controllers;


use common\helpers\ParamsHelper;
use common\models\AccessToken;
use getjump\Vk\Auth;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Class ConnectController
 * @package frontend\controllers
 */
class ConnectController extends Controller
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Список токенов пользователя.
     * @return string
     */
    public function actionIndex()
    {
        $model = AccessToken::find()
            ->isOwnedBy(\Yii::$app->user->id)
            ->isActive()
            ->orderBy(['expires_at' => SORT_DESC])
            ->one();

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * Инициализация авторизации по стандарту oAuth2.0
     * @return \yii\web\Response
     */
    public function actionAuth()
    {
        $query = AccessToken::find()
            ->isOwnedBy(\Yii::$app->user->id)
            ->isActive()
            ->isNotExpired();

        if ($query->exists()) {
            \Yii::$app->session->addFlash('warning', 'У вас уже есть активный токен.');
            return $this->redirect(['index']);
        }

        $token = new AccessToken();
        $token->user_id = \Yii::$app->user->id;
        $token->state = \Yii::$app->security->generateRandomString(8);

        if (!$token->save()) {
            \Yii::$app->session->addFlash('danger', 'Не удалось сохранить заявку на токен.');
            return $this->redirect(['index']);
        }

        $auth = $this->getAuthInstance();
        $auth->setState($token->state);

        return $this->redirect($auth->getUrl());
    }

    /**
     * Завершение oAuth 2.0 авторизации.
     * @param null|string $code Временный код, полученный после прохождения авторизации.
     * @param null|string $state Ключ состояния. Нужен для валидации подключения пользователя.
     * @param null|string $error Ключ ошибки.
     * @param null|string $error_description Подробное описание ошибки.
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionFinish($code = null, $state = null, $error = null, $error_description = null)
    {
        if (isset($code) && isset($state)) {
            $token = AccessToken::find()->where([
                'state' => $state,
                'status' => AccessToken::STATUS_NEW
            ])->one();

            if ($token === null) {
                \Yii::$app->session->addFlash('danger', 'Заявка на токен не найдена.');
                return $this->redirect(['index']);
            }

            $auth = $this->getAuthInstance();

            $response = $auth->getToken($code);

            if (false === $response) {
                \Yii::$app->session->addFlash('danger', 'Не удалось получить токен.');
                return $this->redirect(['index']);
            }

            $token->token = $response->token;
            $token->expires_at = time() + $response->expiresIn;
            $token->account_id = $response->userId;
            $token->status = AccessToken::STATUS_GRANTED;

            if (!$token->save()) {
                \Yii::$app->session->addFlash('danger', 'Не удалось сохранить заявку: ' . var_export($token->errors));
                return $this->redirect(['index']);
            }

            \Yii::$app->session->addFlash('success', 'Доступ успешно получен.');
            \Yii::$app->session->addFlash('info', var_export($response, true));

            return $this->redirect(['index']);
        }

        if (isset($error)) {
            throw new BadRequestHttpException($error_description);
        }

        return $this->redirect(['index']);
    }

    /**
     * Возвращает объект, отвечающий за авторизацию аккаунта в приложении.
     * @return Auth
     */
    private function getAuthInstance()
    {
        $redirect_uri = \Yii::$app->frontendUrlManager->createAbsoluteUrl(['/connect/finish']);
        return Auth::getInstance()
            ->setAppId(ParamsHelper::get(['api', 'vk', 'app_id']))
            ->setSecret(ParamsHelper::get(['api', 'vk', 'api_secret']))
            ->setVersion(ParamsHelper::get(['api', 'vk', 'version'], '5.63'))
            ->setRedirectUri($redirect_uri);
    }
}