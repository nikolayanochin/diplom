<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 01.05.2017
 * Time: 16:06
 */

namespace frontend\modules\task\controllers;


use common\models\AccessToken;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class ParseController
 * @package frontend\modules\task\controllers
 */
class ParseController extends Controller
{
    public function actionCreate($identifier)
    {
        $api = \Yii::$app->api->vk;

        if ($api === null) {
            throw new InvalidConfigException('В данный момент API ВКонтакте недоступен.');
        }

        $token = AccessToken::find()
            ->where([
                'user_id' => \Yii::$app->user->id,
                'status' => AccessToken::STATUS_GRANTED
            ])
            ->andWhere(['>', 'expires_at', time()])
            ->one();

        if ($token === null) {
            \Yii::$app->session->addFlash('danger', 'Необходимо получить токен.');
            return $this->redirect(['/connect/vk/index']);
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $api->callUsersGet($token->token, $identifier);
    }
}