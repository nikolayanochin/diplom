<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 14:13
 */

namespace frontend\modules\connect\controllers;


use common\helpers\DataHelper;
use common\integrations\helpers\SocialNetwork;
use common\models\AccessToken;
use common\models\Account;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Class VkController
 * @package frontend\modules\connect\controllers
 */
class VkController extends Controller
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Страница статуса авторизации в ВК.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Редирект на oAuth 2.0 авторизацию в ВК.
     */
    public function actionAuth()
    {
        $api = \Yii::$app->api->vk;

        if ($api === null) {
            \Yii::$app->session->addFlash('danger', 'В данный момент авторизация недоступна.');
            return $this->redirect(['index']);
        }

        $token = AccessToken::find()
            ->where([
                'user_id' => \Yii::$app->user->id,
                'social_network_id' => SocialNetwork::VK_ID,
                'status' => AccessToken::STATUS_GRANTED,
            ])
            ->andWhere(['>', 'expires_at', time()])
            ->one();

        if ($token !== null) {
            \Yii::$app->session->addFlash('warning', 'У вас уже есть активный токен.');
            return $this->redirect(['index']);
        }

        $token = new AccessToken();
        $token->user_id = \Yii::$app->user->id;
        $token->social_network_id = SocialNetwork::VK_ID;
        $token->state = \Yii::$app->security->generateRandomString(8);

        if (!$token->save()) {
            \Yii::$app->session->addFlash('danger', 'Не удалось сохранить заявку на токен.');
            return $this->redirect(['index']);
        }

        $redirect_uri = \Yii::$app->frontendUrlManager->createAbsoluteUrl(['/connect/vk/finish']);
        $state = $token->state;
        $scope = 'offline';

        $auth_url = $api->getAuthUrl($redirect_uri, $state, $scope, YII_DEBUG);

        return $this->redirect($auth_url);
    }

    /**
     * Завершение oAuth 2.0 авторизации аккаунта ВК.
     * @param null|string $code Временный код, полученный после прохождения авторизации.
     * @param null|string $state Ключ состояния. Нужен для валидации подключения пользователя.
     * @param null|string $error Ключ ошибки.
     * @param null|string $error_description Подробное описание ошибки.
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionFinish($code = null, $state = null, $error = null, $error_description = null)
    {
        if (isset($code) && isset($state)) {
            $api = \Yii::$app->api->vk;

            $token = AccessToken::find()->where([
                'social_network_id' => SocialNetwork::VK_ID,
                'state' => $state,
                'status' => AccessToken::STATUS_NEW
            ])->one();

            if ($token === null) {
                \Yii::$app->session->addFlash('danger', 'Заявка на токен не найдена.');
                return $this->redirect(['index']);
            }

            $redirect_uri = \Yii::$app->frontendUrlManager->createAbsoluteUrl(['/connect/vk/finish']);

            $response = $api->getAccessToken($code, $redirect_uri);

            $token->token = ArrayHelper::getValue($response, 'access_token');
            $token->expires_at = time() + ArrayHelper::getValue($response, 'expires_in');

            /** @var Account|null $account */
            $account = DataHelper::fetchModel(Account::className(), [
                'social_network_id' => SocialNetwork::VK_ID,
                'identifier' => (string)ArrayHelper::getValue($response, 'user_id'),
            ]);

            if ($account === null) {
                \Yii::$app->session->addFlash('danger', 'Не удалось сохранить аккаунт: ' . var_export(ArrayHelper::getValue($response, 'user_id')));
                return $this->redirect(['index']);
            }

            $token->account_id = $account->id;
            $token->status = AccessToken::STATUS_GRANTED;

            if (!$token->save()) {
                \Yii::$app->session->addFlash('danger', 'Не удалось сохранить заявку: ' . var_export($token->errors));
                return $this->redirect(['index']);
            }

            \Yii::$app->session->addFlash('success', 'Доступ успешно получен.');
            \Yii::$app->session->addFlash('info', var_export($response, true));

            return $this->redirect(['index']);
        }

        if (isset($error)) {
            throw new BadRequestHttpException($error_description);
        }

        return $this->goHome();
    }
}