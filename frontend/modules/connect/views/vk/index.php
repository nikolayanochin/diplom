<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 15:09
 */

use yii\helpers\Html;

/** @var \yii\web\View $this */
$this->title = 'Авторизация в ВК';
?>

<div class="text-center">
    <h1><?= $this->title ?></h1>

    <?= Html::a('Пройти авторизацию', ['auth'], ['class' => 'btn btn-primary btn-lg']) ?>
</div>

