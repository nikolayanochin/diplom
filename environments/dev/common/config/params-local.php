<?php
return [
    'url' => [
        'frontend' => 'http://tardience.local/',
        'backend' => 'http://admin.tardience.local/',
    ],
    'api' => [
        'vk' => [
            'app_id' => '',
            'api_secret' => ''
        ]
    ]
];
