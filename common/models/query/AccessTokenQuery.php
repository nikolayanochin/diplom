<?php

namespace common\models\query;

use common\models\AccessToken;

/**
 * This is the ActiveQuery class for [[\common\models\AccessToken]].
 *
 * @see \common\models\AccessToken
 */
class AccessTokenQuery extends \yii\db\ActiveQuery
{
    /**
     * @param $user_id
     * @return AccessTokenQuery
     */
    public function isOwnedBy($user_id)
    {
        return $this->andWhere(['user_id' => $user_id]);
    }

    /**
     * @return AccessTokenQuery
     */
    public function isActive()
    {
        return $this->andWhere(['status' => AccessToken::STATUS_GRANTED]);
    }

    /**
     * @return AccessTokenQuery
     */
    public function isNotExpired()
    {
        return $this->andWhere(['>', 'expires_at', time()]);
    }

    /**
     * @inheritdoc
     * @return \common\models\AccessToken[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\AccessToken|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
