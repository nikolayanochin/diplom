<?php

namespace common\models;

use common\models\query\AccountQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account".
 *
 * @property string $id
 * @property integer $social_network_id
 * @property string $identifier
 * @property string $display_name
 * @property string $description
 * @property string $first_name
 * @property string $last_name
 * @property integer $gender
 * @property string $birth_date
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $zip
 *
 * @property AccessToken[] $accessTokens
 */
class Account extends ActiveRecord
{
    /** @inheritdoc */
    public static function tableName()
    {
        return 'account';
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            [['social_network_id', 'gender'], 'integer'],
            [['identifier'], 'required'],
            [['identifier', 'description', 'birth_date', 'email', 'phone', 'address', 'country', 'region', 'city', 'zip'], 'string', 'max' => 255],
            [['display_name', 'first_name', 'last_name'], 'string', 'max' => 32],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'social_network_id' => 'Социальная сеть',
            'identifier' => 'Идентификатор аккаунта в социальной сети',
            'display_name' => 'Отображаемое имя',
            'description' => 'Биография',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'gender' => 'Пол',
            'birth_date' => 'Дата рождения',
            'email' => 'Почта',
            'phone' => 'Телефон',
            'address' => 'Полный адрес',
            'country' => 'Страна',
            'region' => 'Регион',
            'city' => 'Город',
            'zip' => 'Индекс',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccessTokens()
    {
        return $this->hasMany(AccessToken::className(), ['account_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountQuery(get_called_class());
    }
}
