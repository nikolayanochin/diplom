<?php

namespace common\models;

use common\models\query\AccountQuery;
use common\models\query\AccountRelationQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_relation".
 *
 * @property string $id
 * @property string $initiator_id
 * @property string $target_id
 *
 * @property Account $initiator
 * @property Account $target
 */
class AccountRelation extends ActiveRecord
{
    /** @inheritdoc */
    public static function tableName()
    {
        return 'account_relation';
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            [['initiator_id', 'target_id'], 'required'],
            [['initiator_id', 'target_id'], 'integer'],
            [['initiator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['initiator_id' => 'id']],
            [['target_id'], 'exist', 'skipOnError' => true, 'targetClass' => Account::className(), 'targetAttribute' => ['target_id' => 'id']],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'initiator_id' => 'Инициатор',
            'target_id' => 'Цель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery|AccountQuery
     */
    public function getInitiator()
    {
        return $this->hasOne(Account::className(), ['id' => 'initiator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery|AccountQuery
     */
    public function getTarget()
    {
        return $this->hasOne(Account::className(), ['id' => 'target_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AccountRelationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountRelationQuery(get_called_class());
    }
}
