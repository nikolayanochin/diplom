<?php

namespace common\models;

use common\models\query\AccessTokenQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "access_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $state
 * @property string $token
 * @property string $account_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $expires_at
 *
 * @property User $user
 * @see AccessToken::getUser()
 */
class AccessToken extends ActiveRecord
{
    const STATUS_DELETED = -1;
    const STATUS_EXPIRED = 0;
    const STATUS_NEW = 1;
    const STATUS_GRANTED = 9;

    /** @inheritdoc */
    public static function tableName()
    {
        return 'access_token';
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            [['status'], 'default', 'value' => self::STATUS_NEW],
            [['user_id', 'state', 'status'], 'required'],
            [['user_id', 'account_id', 'status', 'created_at', 'updated_at', 'expires_at'], 'integer'],
            [['state'], 'string', 'max' => 32],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'state' => 'Случайный ключ для определения принадлежности',
            'token' => 'Ключ доступа',
            'account_id' => 'Аккаунт',
            'status' => 'Статус доступа',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'expires_at' => 'Дата окончания',
        ];
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className()
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\AccessTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccessTokenQuery(get_called_class());
    }

    /**
     * Просрочен ли токен.
     * @return bool|null
     */
    public function isExpired()
    {
        return $this->expires_at === null ? null : $this->expires_at < time();
    }
}
