<?php
/**
 * Created by PhpStorm.
 * User: unregistered
 * Date: 15.03.2017
 * Time: 21:04
 */

namespace common\components;


use yii\web\UrlManager;
use common\helpers\ParamsHelper;

class FrontendUrlManager extends UrlManager
{
    /** @inheritdoc */
    public function init()
    {
        parent::init();
        $this->setBaseUrl(ParamsHelper::get(['url', 'frontend']));
    }
}