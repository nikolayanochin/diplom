<?php
return [
    'name' => 'Tardience',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'sourceLanguage' => 'source',
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'currencyCode' => 'RUB',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'dateFormat' => 'php:d.m.Y',
            'timeFormat' => 'php:H:i:s',
            'timeZone' => 'UTC',
        ],
        'frontendUrlManager' => [
            'class' => 'common\components\FrontendUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => []
        ],
        'rabbitmq' => [
            'class' => 'mikemadisonweb\rabbitmq\Configuration',
            'producers' => [
                'initialize_task' => [
                    'connection' => 'default',
                    'exchange_options' => [
                        'name' => 'initialize_task',
                        'type' => 'direct',
                        'durable' => false,
                    ],
                ],
            ],
            'consumers' => [
                'initialize_task' => [
                    'connection' => 'default',
                    'qos_options' => [
                        'prefetch_size' => 0,
                        'prefetch_count' => 1,
                        'global' => false,
                    ],
                    'exchange_options' => [
                        'name' => 'initialize_task',
                        'type' => 'direct',
                    ],
                    'queue_options' => [
                        'name' => 'initialize_task',
                        'routing_keys' => ['initialize_task'],
                        'durable' => true,
                        'auto_delete' => false,
                    ],
                    'callback' => \console\queue\InitializeTaskConsumer::className(),
                ],
            ],
        ],
    ],
];
