<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@root', dirname(dirname(__DIR__)));
Yii::setAlias('@bower', dirname(dirname(__DIR__)) . '/vendor/bower');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
