<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 14:22
 */

namespace common\integrations\interfaces;

/**
 * Interface APIInterface
 * @package common\integrations\interfaces
 */
interface RequestInterface
{
    const REQUEST_METHOD_GET = 'get';
    const REQUEST_METHOD_POST = 'post';
}