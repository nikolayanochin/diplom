<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 14:22
 */

namespace common\integrations\interfaces;

/**
 * Interface APIInterface
 * @package common\integrations\interfaces
 */
interface APIInterface
{
    /**
     * Конструктор Api.
     * @param string $app_id
     * @param string $api_secret
     * @param null|string $api_version
     */
    public function __construct($app_id, $api_secret, $api_version = null);

    /**
     * Ссылка на авторизацию пользователя в приложении.
     * @param string $redirect_uri Адрес, на который будет передан code.
     * @param string $state Произвольная строка, которая будет возвращена вместе с результатом авторизации.
     * @param string $scope Запрашиваемые разрешения.
     * @param bool $is_test_mode
     * @return string
     */
    public function getAuthUrl($redirect_uri, $state, $scope = '', $is_test_mode = false);

    /**
     * Получение ключа доступа по коду авторизации.
     * @param string $code Временный код, полученный после прохождения авторизации.
     * @param string $redirect_uri URL, который использовался при получении code на первом этапе авторизации. Должен быть аналогичен переданному при авторизации.
     * @return mixed
     */
    public function getAccessToken($code, $redirect_uri);

    /**
     * Вернет массив с информацией по указанным аккаунтам.
     * @param string $access_token Токен
     * @param string $user_ids Список идентификаторов аккаунтов, по которым необходимо получить информацию.
     * @return mixed
     */
    public function callUsersGet($access_token, $user_ids);
}