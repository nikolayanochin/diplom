<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 14:18
 */

namespace common\integrations\components;


use common\helpers\ParamsHelper;
use common\integrations\helpers\SocialNetwork;
use yii\base\Component;
use common\integrations\vk\Api as VkAPI;
use yii\helpers\ArrayHelper;

/**
 * Class Api
 * @package common\integrations\components
 *
 * @property \common\integrations\interfaces\APIInterface|null $vk
 */
class Api extends Component
{
    /** @var array */
    public $configs;

    /** @var array */
    private $api_collection;

    /** @inheritdoc */
    public function init()
    {
        parent::init();

        $this->api_collection = [];

        $vk_app_id = ParamsHelper::get(['api', 'vk', 'app_id']);
        $vk_api_secret = ParamsHelper::get(['api', 'vk', 'api_secret']);
        $vk_api_version = ParamsHelper::get(['api', 'vk', 'api_version']);

        if (!empty($vk_app_id) && !empty($vk_api_secret)) {
            $this->api_collection[SocialNetwork::VK_ID] = new VkAPI($vk_app_id, $vk_api_secret, $vk_api_version);
        }
    }

    public function isAvailable($key)
    {
        return key_exists($this->api_collection, $key);
    }

    public function vkIsAvailable()
    {
        return $this->isAvailable(SocialNetwork::VK_ID);
    }

    public function getVk()
    {
        return ArrayHelper::getValue($this->api_collection, SocialNetwork::VK_ID);
    }
}