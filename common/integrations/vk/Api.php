<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 13:20
 */

namespace common\integrations\vk;


use common\integrations\interfaces\APIInterface;

class Api extends Request implements APIInterface
{
    const HOST_AUTH = 'https://oauth.vk.com';
    const  HOST_API = 'https://api.vk.com/method';

    const RESPONSE_TYPE_CODE = 'code';

    const AUTH_METHOD_AUTHORIZE = 'authorize';
    const AUTH_METHOD_ACCESS_TOKEN = 'access_token';

    const API_METHOD_GET_USER_SETTINGS = 'getUserSettings';
    const API_METHOD_USERS_GET = 'users.get';


    /**
     * VK идентификатор приложения.
     * @var string
     */
    private $app_id;

    /**
     * VK секретный ключ приложения.
     * @var string
     */
    private $api_secret;

    /**
     * API версия. Если null, то используется последняя.
     * @var string
     */
    private $api_version;


    /** @inheritdoc */
    public function __construct($app_id, $api_secret, $api_version = '5.63')
    {
        $this->app_id = $app_id;
        $this->api_secret = $api_secret;
        $this->api_version = $api_version;

        parent::__construct();
    }

    /** @inheritdoc */
    public function getAuthUrl($redirect_uri, $state, $scope = '', $is_test_mode = false)
    {
        $params = [
            'client_id' => $this->app_id,
            'scope' => $scope,
            'redirect_uri' => $redirect_uri,
            'response_type' => self::RESPONSE_TYPE_CODE,
            'state' => $state,
        ];

        if ($is_test_mode) {
            $params['test_mode'] = 1;
        }


        return self::makeUrl(self::HOST_AUTH, self::AUTH_METHOD_AUTHORIZE, $params);
    }

    /** @inheritdoc */
    public function getAccessToken($code, $redirect_uri)
    {
        $parameters = [
            'client_id' => $this->app_id,
            'client_secret' => $this->api_secret,
            'code' => $code,
            'redirect_uri' => $redirect_uri
        ];

        $url = self::makeUrl(self::HOST_AUTH, self::AUTH_METHOD_ACCESS_TOKEN, $parameters);
        return $this->call($url);
    }


    /**
     * Возвращает настройки пользователя.
     * @param   string $access_token
     * @return  bool
     */
    public function getUserSettings($access_token)
    {
        $url = self::makeUrl(self::HOST_API, self::API_METHOD_GET_USER_SETTINGS);
        return $this->call($url, ['access_token' => $access_token]);
    }

    /** @inheritdoc */
    public function callUsersGet($access_token, $user_ids)
    {
        $url = self::makeUrl(self::HOST_API, self::API_METHOD_USERS_GET);
        return $this->call($url, [
            'user_ids' => $user_ids,
            'fields' => 'sex, bdate, city, country, home_town, domain, contacts, site, education, universities, schools, personal, activities, interests, music, movies, tv, books, games, about, quotes, screen_name, career, military',
            'access_token' => $access_token
        ]);
    }
}