<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 30.04.2017
 * Time: 13:20
 */

namespace common\integrations\vk;


use common\integrations\interfaces\RequestInterface;

class Request implements RequestInterface
{
    /** @var string */
    const USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36';

    /** @var resource|false cURL обработчик */
    private $curl_handler;

    /** @var string|false Заголовок ответа */
    private $_header;
    /** @var string|false Тело ответа */
    private $_body;
    /** @var int Код ответа */
    private $_code;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->curl_handler = curl_init();

        curl_setopt($this->curl_handler, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($this->curl_handler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl_handler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl_handler, CURLOPT_HEADER, true);
    }

    /**
     * @return false|string
     */
    public function getHeader()
    {
        return $this->_header;
    }

    /**
     * @return false|string
     */
    public function getBody()
    {
        return $this->_body;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Генерирует ссылку.
     * @param string $host
     * @param string $endpoint
     * @param array $params
     * @return string
     */
    protected static function makeUrl($host, $endpoint = '', $params = [])
    {
        $url = $host;

        if (!empty($endpoint)) {
            $url .= '/' . $endpoint;
        }

        if (!empty($params)) {
            $params = array_filter($params);
            $url .= "?" . http_build_query($params);
        }

        return $url;
    }

    /**
     * @param string $url
     * @param array $post_fields
     * @return mixed Содержание body после JSON-декодирования
     */
    protected function call($url, $post_fields = [])
    {
        list($this->_header, $this->_body, $this->_code) = $this->request($url, $post_fields);

        return $this->_body;
    }

    /**
     * Осуществляет запрос к API.
     * @param string $url
     * @param array $post_fields
     * @return array
     */
    protected function request($url, $post_fields = [])
    {
        curl_setopt($this->curl_handler, CURLOPT_URL, $url);

        if (is_array($post_fields) && !empty($post_fields)) {
            curl_setopt($this->curl_handler, CURLOPT_POST, true);
            curl_setopt($this->curl_handler, CURLOPT_POSTFIELDS, $post_fields);
        }

        $response = curl_exec($this->curl_handler);
        $response_code = curl_getinfo($this->curl_handler, CURLINFO_HTTP_CODE);

        $header_len = curl_getinfo($this->curl_handler, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_len);
        $body = substr($response, $header_len);

        // Преобразуем bigint к строке.
        $body = preg_replace('/:\s?(\d{14,})/', ': "${1}"', $body);
        $body = json_decode($body, true, 512, JSON_BIGINT_AS_STRING);

        if (is_array($post_fields) && !empty($post_fields)) {
            curl_setopt($this->curl_handler, CURLOPT_POST, false);
            curl_setopt($this->curl_handler, CURLOPT_POSTFIELDS, null);
        }

        return [$header, $body, $response_code];
    }
}