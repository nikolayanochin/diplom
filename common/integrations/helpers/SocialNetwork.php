<?php
/**
 * Created with love in Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 01.05.2017
 * Time: 15:25
 */

namespace common\integrations\helpers;


class SocialNetwork
{
    /** @var int Идентификатор социальной сети ВКонтакте */
    const VK_ID = 0;
    /** @var int Идентификатор социальной сети Facebook */
    const FB_ID = 1;
    /** @var int Идентификатор социальной сети Одноклассники */
    const OK_ID = 2;
    /** @var int Идентификатор социальной сети Instagram */
    const IN_ID = 3;
}