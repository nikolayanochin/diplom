<?php
/**
 * Created with love by Kodelnaya.
 * Author: Dudarev Ilia
 * Email: ilya@kodelnya.ru
 * Phone: +7 906 780 3210
 * Date: 23.09.2016
 * Time: 18:43
 */

namespace common\helpers;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class DataHelper
 * @package common\helpers
 */
class DataHelper
{
    /** @var ActiveRecord[] */
    private static $_sources = [];

    /**
     * Ищет запись в БД по указанным параметрам, если не нашел, то создает ее.
     * В результате возвращает экземляр записи.
     * @param $className
     * @param $params
     * @return array|null|ActiveRecord
     */
    public static function fetchModel($className, $params)
    {
        /** @var ActiveRecord $class */
        $class = new $className;

        if (!($class instanceof ActiveRecord)) {
            throw new \BadMethodCallException('Method work only with ActiveRecord');
        }

        $key = implode('-', array_merge([$className], array_values($params)));
        $key = str_replace('.', '-', $key);
        $model = ArrayHelper::getValue(self::$_sources, $key);

        if ($model === null) {
            $model = $class::find()->where($params)->one();
        }

        if ($model === null) {
            /** @var ActiveRecord $model */
            $model = new $className($params);

            if (!$model->save(false)) {
                return null;
            }

            $model->refresh();
        }

        self::$_sources[$key] = $model;

        return $model;
    }
}