<?php
/**
 * Created by PhpStorm.
 * User: unregistered
 * Date: 15.03.2017
 * Time: 21:05
 */

namespace common\helpers;


use yii\helpers\ArrayHelper;

class ParamsHelper
{
    /**
     * @param array|callable|string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        return ArrayHelper::getValue(\Yii::$app->params, $key, $default);
    }
}